<?php
// +----------------------------------------------------------------------
// | ThinkPHP5.1+ Addons [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Hawk <hawk@sharebox.cc>
// +----------------------------------------------------------------------

namespace think\addons;

use think\facade\Env;
use think\facade\Config;
use think\Loader;
use think\Request;
use think\facade\Lang;

/**
 * 插件基类控制器
 * Class Controller
 * @package think\addons
 */
class Controller extends \think\Controller
{
    // 当前插件操作
    protected $addon = null;
    protected $controller = null;
    protected $action = null;
    // 模板配置信息
    protected $config = [
        'type'         => 'Think',
        'view_path'    => '',
        'view_suffix'  => 'html',
        'strip_space'  => true,
        'view_depr'    => DIRECTORY_SEPARATOR,
        'tpl_begin'    => '{',
        'tpl_end'      => '}',
        'taglib_begin' => '{',
        'taglib_end'   => '}',
    ];

    /**
     * 构造方法
     * @param Request $request Request对象
     * @access public
     */
    public function __construct()
    {
        // 初始化配置信息
        $this->config = Config::get('template.') ?: $this->config;
        // 处理路由参数
        $routen =  request()->route();
        $b='';
        foreach($routen as $k=>$v){
            $b.=$k."\\".$v;
            $b.="\\";
        }
        $param = explode('\\', $b);
        // 是否自动转换控制器和操作名
        $convert = Config::get('url_convert');
        // 格式化路由的插件位置
        $this->action = $convert ? strtolower($param[3]) : $param[3];
        $this->controller = $convert ? strtolower($param[2]) : $param[2];
        $this->addon = $convert ? strtolower($param[1]) : $param[1];
        // 生成view_path
        $view_path = $this->config['view_path'] ?: 'view';
        // 重置配置
        Config::set('template.view_path', Env::get('addon_path') . $this->addon . DIRECTORY_SEPARATOR . $view_path . DIRECTORY_SEPARATOR);
        // 加载系统语言包
        Lang::load(Env::get('addon_path'). $this->addon . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . lang::range() . '.php');
        parent::__construct();
    }

    /**
     * 加载模板输出
     * @access protected
     * @param string $template 模板文件名
     * @param array  $vars 模板输出变量
     * @param array  $replace 模板替换
     * @param array  $config 模板参数
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $replace = [], $config = [])
    {
        $controller = Loader::parseName($this->controller);
        if ('think' == strtolower($this->config['type']) && $controller && 0 !== strpos($template, '/')) {
            $depr = $this->config['view_depr'];
            $template = str_replace(['/', ':'], $depr, $template);
            if ('' == $template) {
                // 如果模板文件名为空 按照默认规则定位
                $template = str_replace('.', DIRECTORY_SEPARATOR, $controller) . $depr . $this->action;
            } elseif (false === strpos($template, $depr)) {
                $template = str_replace('.', DIRECTORY_SEPARATOR, $controller) . $depr . $template;
            }
        }
        return $this->view->fetch($template, $vars, $replace, $config);
    }
}
