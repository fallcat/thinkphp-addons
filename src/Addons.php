<?php
// +----------------------------------------------------------------------
// | ThinkPHP5.1+ Addons [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Hawk <hawk@sharebox.cc>
// +----------------------------------------------------------------------

namespace think;

use think\facade\Config;
use think\facade\View;
use think\facade\Lang;
use think\facade\Env;

/**
 * 插件基类
 * Class Addons
 * @author Byron Sampson <xiaobo.sun@qq.com>
 * @package think\addons
 */
abstract class Addons
{
    /**
     * 视图实例对象
     * @var view
     * @access protected
     */
    protected $view = null;

    // 当前错误信息
    protected $error;

    /**
     * $info = [
     *  'name'          => 'Test',
     *  'title'         => '测试插件',
     *  'description'   => '用于thinkphp5的插件扩展演示',
     *  'status'        => 1,
     *  'author'        => 'byron sampson',
     *  'version'       => '0.1'
     * ]
     */
    public $info = [];
    public $addons_path = '';
    public $config_file = '';

    /**
     * 架构函数
     * @access public
     */
    public function __construct()
    {
        // 获取当前插件名
        $this->addons_name = $this->getName();
        // 获取当前插件目录
        $this->addons_path = Env::get('addon_path') . $this->addons_name . DIRECTORY_SEPARATOR;
        // 读取当前插件配置信息
        if (is_file($this->addons_path . 'config.php')) {
            $this->config_file = $this->addons_path . 'config.php';
        }
        // 加载插件语言包
        Lang::load($this->addons_path . 'lang' . DIRECTORY_SEPARATOR . lang::range() . '.php');
        // 初始化视图模型
        $config = ['view_path' => $this->addons_path];
        $config = array_merge(Config::get('template.'), $config);
        $this->view = app('view')->init($config);
    }

    /**
     * Notes:通过文件获取插件的默认配置数组
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:43
     * @param string $name 可选插件名
     * @return array|mixed
     *
     */
    final public function getConfig($name = '')
    {
        static $_config = [];
        if (empty($name)) {
            $name = $this->getName();
        }
        if (isset($_config[$name])) {
            return $_config[$name];
        }
        $map['name'] = $name;
        $map['status'] = 1;
        $config = [];
        if (is_file($this->config_file)) {
            $temp_arr = include $this->config_file;
            foreach ($temp_arr as $key => $value) {
                if ($value['type'] == 'group') {
                    foreach ($value['options'] as $gkey => $gvalue) {
                        foreach ($gvalue['options'] as $ikey => $ivalue) {
                            $config[$ikey] = $ivalue['value'];
                        }
                    }
                } else {
                    $config[$key] = $temp_arr[$key]['value'];
                }
            }
            unset($temp_arr);
        }
        $_config[$name] = $config;

        return $config;
    }

    /**
     * Notes:通过数据库plugins获取插件的新配置数组
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:43
     * @param string $name
     * @return mixed
     *
     */
    final public function getAddonsConfig($name = '')
    {
        if (empty($name)) {
            $name = $this->getName();
        }
        if (isset($_config[$name])) {
            return $_config[$name];
        }
        $map['name'] = $name;
        $map['status'] = 1;
        $_config = db('plugins')->where($map)->value('config');
        $config = json_decode($_config, true);
        return $config;
    }

    /**
     * Notes:获取当前模块名
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:44
     * @return string
     *
     */
    final public function getName()
    {
        $data = explode('\\', get_class($this));
        return strtolower(array_pop($data));
    }

    /**
     * Notes:检查配置信息是否完整
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:44
     * @return bool
     *
     */
    final public function checkInfo()
    {
        $info_check_keys = ['name', 'title', 'description', 'author', 'version', 'plugin_url', 'author_url', 'status'];
        foreach ($info_check_keys as $value) {
            if (!array_key_exists($value, $this->info)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Notes:检查数据库是否安装
     * User: hawk
     * Date: 2019/1/8
     * Time: 9:36
     * @return mixed
     */
    final public function checkDatabase()
    {
        $plugin_name = $this->addons_name;
        $prefix = Config::get('database.prefix');
        $exist = Db::query('show tables like "'.$prefix.'plugin_'.$plugin_name.'"');
        return true;
    }

    /**
     * Notes:刷新插件配置信息
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:45
     * @return bool|void
     * @throws Exception
     *
     */
    public static function refresh()
    {

        $file = Env::get('config_path') . '/addons.php';
        $config = get_addon_autoload_config(true);
        if ($config['autoload']) {
            return;
        } else {
            $config['autoload'] = false;
        }

        if ($handle = fopen($file, 'w')) {
            fwrite($handle, "<?php\n\n" . "return " . var_export($config, true) . ";");
            fclose($handle);
        } else {
            throw new Exception("文件没有写入权限");
        }
        return true;
    }
    /**
     * 加载模板和页面输出 可以返回输出内容
     * @access public
     * @param string $template 模板文件名或者内容
     * @param array  $vars 模板输出变量
     * @param array  $replace 替换内容
     * @param array  $config 模板参数
     * @return mixed
     * @throws \Exception
     */
    public function fetch($template = '', $vars = [], $replace = [], $config = [])
    {
        if (!is_file($template)) {
            $template = '/' . $template;
        }
        // 关闭模板布局
        $this->view->engine->layout(false);

        echo $this->view->fetch($template, $vars, $replace, $config);
    }

    /**
     * 渲染内容输出
     * @access public
     * @param string $content 内容
     * @param array  $vars 模板输出变量
     * @param array  $replace 替换内容
     * @param array  $config 模板参数
     * @return mixed
     */
    public function display($content, $vars = [], $replace = [], $config = [])
    {
        // 关闭模板布局
        $this->view->engine->layout(false);

        echo $this->view->display($content, $vars, $replace, $config);
    }

    /**
     * 渲染内容输出
     * @access public
     * @param string $content 内容
     * @param array  $vars 模板输出变量
     * @return mixed
     */
    public function show($content, $vars = [])
    {
        // 关闭模板布局
        $this->view->engine->layout(false);

        echo $this->view->fetch($content, $vars, [], [], true);
    }

    /**
     * 模板变量赋值
     * @access protected
     * @param mixed $name 要显示的模板变量
     * @param mixed $value 变量的值
     * @return void
     */
    public function assign($name, $value = '')
    {
        $this->view->assign($name, $value);
    }

    /**
     * 获取当前错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Notes:钩子方法不存在，执行插件类:run方法
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:45
     * @param $param
     *
     */
    public function run($param)
    {
        echo 'execute run method, maybe hook method not exists.';
        cache('addons', null);
        cache('hooks', null);
    }

    /**
     * Notes:通过数据库判断是否插件已经安装
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:45
     * @param $name
     * @return bool
     *
     */
    public function isinstall($name)
    {
        $where = [
            ['name', '=', $name],
            ['status', '=', 1]
        ];

        $pluginCount = db('plugins')->where($where)->count();
        if ($pluginCount > 0) {
            return false;
        }

        return true;
    }

    /**
     * Notes:通过lock文件判断是否插件已经安装
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:46
     * @return bool
     *
     */
    public function is_installed()
    {
        static $IsInstalled;
        if (empty($IsInstalled)) {
            $IsInstalled = file_exists($this->addons_path . '/data/install.lock');
        }
        return $IsInstalled;
    }

    /**
     * Notes:必须实现，插件初始安装时，插件系统会调用此方法
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:46
     * @return mixed
     *
     */
    abstract public function install();

    /**
     * Notes:必须实现，插件卸载时，插件系统会调用此方法
     * Author: hawk <hawk@shareboxi.com>
     * Date: 2019/1/8 10:46
     * @return mixed
     *
     */
    abstract public function uninstall();

}
